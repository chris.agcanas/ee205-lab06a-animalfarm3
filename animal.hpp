///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   01 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN};

class Animal {
public:
	enum Gender gender;
	string      species;

   //Constructor and Destructor
	Animal();
   ~Animal();

   virtual const string speak() = 0;
	
	void printInfo();
  
   //Random Attribute Generators
   static const Gender  getRandomGender();
   static const Color   getRandomColor();
   static const bool    getRandomBool();
   static const float   getRandomWeight( const float from, const float to);
   static const string  getRandomName();

	static string colorName  (enum Color color);
	static string genderName (enum Gender gender);
};

} // namespace animalfarm
