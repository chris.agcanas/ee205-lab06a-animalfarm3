///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

//header files
#include "animal.hpp"
#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main(){
   cout << "Welcome to Animal Farm 3" << endl;
  
   //Array Containers
   array<Animal* , 30> animalArray;
   animalArray.fill(NULL);
   for(int i =0; i < 25; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }
   
   //first endl to separate line from constructors
   cout << endl << "Array of Animals:" << endl;
   cout << "   Is it empty: " << boolalpha << animalArray.empty() << endl;
   cout << "   Number of elements: " << animalArray.size() << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;

   //Making each animal in array speak
   for (Animal* animal : animalArray){
      if(animal != NULL){
         cout << animal->speak() << endl;
      }
   }
   
   //Clearing Array
   for(int i =0; i < 25; i++){
      delete animalArray[i]; 
      animalArray[i] = NULL;
   }


   //List Containers
   list<Animal* > animalList;
   for(int i = 0; i < 25; i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   
   cout << endl << "List of Animals:" << endl;
   cout << "   Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "   Number of elements: " << animalList.size() << endl;
   cout << "   Max size: " << animalList.max_size() << endl;

   for (Animal* animal : animalList){
      if(animal != NULL){
         cout << animal->speak() << endl;
      }
   }

   //Clearing list
   for (Animal* animal: animalList){
      delete animal;
   }
   animalList.clear(); 

   return 0;
}
