///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animalfactory.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Chistopher Agcanaas <agcanas8@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   12 Mar 2021
///////////////////////////////////////////////////////////////////////////////
#pragma once
//Header files
#include "animal.hpp"

using namespace std;

namespace animalfarm {
   class AnimalFactory{
      public:
         static Animal* getRandomAnimal();
   };

} // namespace animalfarm
