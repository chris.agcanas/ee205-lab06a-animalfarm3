//Test file for random generator

#include <iostream>
#include <string>

//header files
#include "animal.hpp"
#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;
using namespace animalfarm;


int main(){
//   class Animal myAnimal;
   
   cout << "Starting test:" << endl;

   //Test for each animal
   Cat      tCat(Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
   Dog      tDog(Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
   Nunu     tNunu(Animal::getRandomBool(), RED, Animal::getRandomGender());
   Aku      tAku(Animal::getRandomWeight(9.2, 20.5), SILVER, Animal::getRandomGender());
   Palila   tPalila(Animal::getRandomName(), YELLOW, Animal::getRandomGender());
   Nene     tNene(Animal::getRandomName(), BROWN, Animal::getRandomGender());

   tCat.printInfo();
   tDog.printInfo();
   tNunu.printInfo();
   tAku.printInfo();
   tPalila.printInfo();
   tNene.printInfo();

   //Test Generator individually   
   cout << "Random Attribute Generator:" << endl;
   cout << "   Gender: " << Animal::genderName(Animal::getRandomGender()) << endl;
   cout << "   Color: " << Animal::colorName(Animal::getRandomColor()) << endl;
   cout << "   Bool: " << std::boolalpha << Animal::getRandomBool() << endl;
   cout << "   Weight: " << Animal::getRandomWeight(9.2, 20.5) << endl;
   cout << "   Name: " << Animal::getRandomName() << endl;
   
   //Testing Factory
   cout << "Testing Factory" << endl;
   for(int i = 0; i<25; i++){
      Animal* a = AnimalFactory::getRandomAnimal();
      cout << a->speak() << endl;
   }

   cout << "End of Test!" << endl;

}
