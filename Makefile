###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

all: main

main.o:  animal.hpp animalfactory.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

#factory
animalfactory.o: animalfactory.cpp animalfactory.hpp
	g++ -c animalfactory.cpp

# test file
test.o: animal.hpp animal.cpp animalfactory.cpp animalfactory.hpp
	g++ -c test.cpp

test: test.cpp test.o animal.o animalfactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o test test.o animal.o animalfactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o


main: main.cpp *.hpp main.o animal.o animalfactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o main main.o animal.o animalfactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	
clean:
	rm -f *.o main
