///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   01 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
// For rand()
#include <stdlib.h>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

   //Animal Constructor
   Animal:: Animal(){
      cout << ".";
      //cout << "." << endl;
   }

   //Destructor
   Animal:: ~Animal(){
      cout << "x";
      //cout << "x" << endl;
   }


   void Animal::printInfo() {
	   cout << "   Species = [" << species << "]" << endl;
	   cout << "   Gender = [" << genderName( gender ) << "]" << endl;
   }

   //Male or Female
   const enum Gender Animal::getRandomGender(){
      //Randomize between 0 or 1
      int gender = rand() %2;

      switch(gender){
         case 0: return MALE; break;
         case 1: return FEMALE; break;
      }
      return MALE; //Should never get here
   } 

   //Random color from Color enum
   const Color Animal::getRandomColor(){
      //randomize 6 colors
      int color = rand() % 6;

      switch(color){
         case 0: return BLACK;   break;
         case 1: return WHITE;   break;
         case 2: return RED;     break;
         case 3: return SILVER;  break;
         case 4: return YELLOW;  break;
         case 5: return BROWN;   break;

      }
      return BLACK;
   }  

   // Random true or false
   const bool Animal::getRandomBool(){
      int tf = rand() % 2;
      
      switch(tf){
         case 0: return true; break;
         case 1: return false; break;

      }
      return true; //should never get here
   }

   //Random Weighet generator:
   const float Animal:: getRandomWeight(const float from, const float to){
      int  range = (int) (to - from);
      float randomWeight = from + (rand()%range);
      return randomWeight;
   }


   //Random Name Generator
   const string Animal::getRandomName(){
      //int length = randomLength(4,9);
      int length = 4 + (rand()%6);
      char randomName[length];
  
      //A = 65 on ASCI
      randomName[0] = (char) (65 + rand()%26); 
   
      for (int i=1; i < length; i++){
         // a = 97 on ASCII
         randomName[i] = (char) (97 + rand()%26);
      }

      return randomName;
   }



   // Decode Gender enum into strings.
   string Animal::genderName (enum Gender gender) {
      switch (gender) {
         case MALE:    return string("Male"); break;
         case FEMALE:  return string("Female"); break;
         case UNKNOWN: return string("Unknown"); break;
      }

      return string("Really, really Unknown");
   }
	
   // Decode Color enum into strings.
   string Animal::colorName (enum Color color) {
      switch(color){
         case BLACK:    return string("Black"); break;
         case WHITE:    return string("White"); break;
         case RED:      return string("Red");   break;
         case SILVER:   return string("Silver");break;
         case YELLOW:   return string("Yellow");break;
         case BROWN:    return string("Brown"); break;

      }
      return string("Unknown");
   };
	
} // namespace animalfarm

